package com.bence.chess

import org.scalatest.{FunSuite, Matchers}

class ChessBoardTest extends FunSuite with Matchers {

  val rows, cols: Int = 6

  /**
    * Q*****
    * **.NN.
    * ***..*
    * ******
    * *R****
    * **...*
    */
  val board = ChessBoard.create(rows, cols, (Queen, 0, 0)::(Knight, 1, 3)::(Knight, 1, 4)::(Rook, 4, 1)::Nil)

  /**
    * This board is solely for test minCoordsToPlace for all chess piece types. As such, pieces may be attacking each
    * other.
    *
    * *K*.
    * Q***
    * **R*
    * *N*B
    */
  val board2 = ChessBoard.create(4, 4, (King, 0, 1)::(Queen, 1, 0)::(Rook, 2, 2)::(Bishop, 3, 3)::(Knight, 3, 1)::Nil)

  test("free places should be (1, 2), (1, 5), (2, 3), (2, 4), (5, 2), (5, 3), (5, 4)") {
    val freePlaces = Set((1, 2), (1, 5), (2, 3), (2, 4), (5, 2), (5, 3), (5, 4))

    for (row <- 0 until rows; col <- 0 until cols) {
      val expectedFree = freePlaces.contains((row, col))
      assert(board.isFree(row, col) == expectedFree,
        s"(${row}, ${col}) is expected to be ${if (expectedFree) "free" else "attacked or occupied"}")
    }
  }

  def testPlaceable(piece: ChessPiece, goodPlaces: Set[(Int, Int)]): Unit =
    for (row <- 0 until rows; col <- 0 until cols) {
      val expectedPlaceable = goodPlaces.contains((row, col))
      assert(board.isOccupiedOrInAttack(piece, row, col) != expectedPlaceable,
        s"A ${piece} should ${if (expectedPlaceable) "" else "NOT "}be placeable at (${row}, ${col})")
    }


  test("a KING can only be placed at (5, 3) or (5, 4)") {
    val goodPlaces = Set((5, 3), (5, 4))
  }

  test("a KNIGHT can only be placed at (1, 5) or (2, 3) or (2, 4) or (5, 2) or (5, 4)") {
    testPlaceable(Knight, Set((1, 5), (2, 3), (2, 4), (5, 2), (5, 4)))
  }

  test("a ROOK can only be placed at (5, 2)") {
    testPlaceable(Rook, Set((5, 2)))
  }

  test("a BISHOP can only be placed at (1, 2) or (1, 5) or (5, 3) or (5, 4)") {
    testPlaceable(Bishop, Set((1, 2), (1, 5), (5, 3), (5, 4)))
  }

  test("no more QUEENs can be placed on the board") {
    testPlaceable(Queen, Set.empty)
  }


  test("nextPosition() should traverse horizontally, starting in the next row when end of row is reached") {
    val positionTraversals = List(
      (0, 0) -> Some((0, 1)),
      (0, 1) -> Some((0, 2)),
      // ...
      (0, 5) -> Some((1, 0)),
      (1, 0) -> Some((1, 1)),
      // ...
      (4, 5) -> Some((5, 0)),
      // ...
      (5, 5) -> None)
    positionTraversals.foreach {
      case (fromPosition, optionalToPosition) =>
        board.nextPosition(fromPosition) should equal(optionalToPosition)
    }
  }

  test("next good place for KING from (0, 0) should be (5, 3)") {
    board.findGoodPlaceFrom(King, (0, 0)) should equal (Some((5, 3)))
  }

  test("next good place for KING from (5, 4) should be (5, 4)") {
    board.findGoodPlaceFrom(King, (0, 0)) should equal (Some((5, 3)))
  }

  test("next good place for KING from (5, 5) should be None") {
    board.findGoodPlaceFrom(King, (5, 5)) should equal (None)
  }

  test("next good place for KNIGHT from (0, 0) should be (1, 5)") {
    board.findGoodPlaceFrom(Knight, (0, 0)) should equal (Some(1, 5))
  }

  test("next good place for KNIGHT from (2, 5) should be (5, 2)") {
    board.findGoodPlaceFrom(Knight, (2, 5)) should equal (Some(5, 2))
  }

  test("next good place for ROOK from (0, 0) should be (5, 2)") {
    board.findGoodPlaceFrom(Rook, (0, 0)) should equal (Some(5, 2))
  }

  test("next good place for QUEEN  from (0, 0) should be None") {
    board.findGoodPlaceFrom(Queen, (0, 0)) should equal (None)
  }

  test("starting with an empty board and placing pieces one by one should result in the same board") {
    val board = ChessBoard.create(4, 4,
      (King, 0, 1) :: (Queen, 1, 1) :: (Rook, 2, 0) :: (Bishop, 2, 1) :: (Knight, 3, 0) :: Nil)
    val boardUsingPlace = ChessBoard.initial(4, 4).
      place(King, 0, 1).
      place(Queen, 1, 1).
      place(Rook, 2, 0).
      place(Bishop, 2, 1).
      place(Knight, 3, 0)
    boardUsingPlace should equal(board)
  }

  test("placement options for a new KING should be evaluated from (0, 1") {
    board2.minCoordsToPlace(King) should equal((0, 1))
  }

  test("placement options for a new QUEEN should be evaluated from (1, 0") {
    board2.minCoordsToPlace(Queen) should equal((1, 0))
  }

  test("placement options for a new ROOK should be evaluated from (2, 2") {
    board2.minCoordsToPlace(Rook) should equal((2, 2))
  }

  test("placement options for a new KNIGHT should be evaluated from (3, 1") {
    board2.minCoordsToPlace(Knight) should equal((3, 1))
  }

  test("placement options for a new BISHOP should be evaluated from (0, 0") {
    board2.minCoordsToPlace(Bishop) should equal((3, 3))
  }

}
