package com.bence.chess

import org.apache.logging.log4j.scala.Logging
import org.scalatest.{FunSuite, Matchers}
import org.scalacheck.Gen
import org.scalacheck.Prop.forAll

class ChessPuzzleSolverTest extends FunSuite with Matchers {

  val (twoKingsOneRockInput, twoKingsOneResults) = Input(rows = 3, columns = 3, kings = 2, rooks = 1) -> Set(
    """
      |K.K
      |...
      |.R.
      |""".stripMargin,
    """
      |K..
      |..R
      |K..
      |""".stripMargin,
      """
      |..K
      |R..
      |..K
      |""".stripMargin,
    """
      |.R.
      |...
      |K.K
      |""".stripMargin)

  val (twoRooksFourKnightsInput, twoRooksFourKnightsResults) = Input(rows = 4, columns = 4, rooks = 2, knights = 4) -> Set(
    """
      |.N.N
      |..R.
      |.N.N
      |R...
      |""".stripMargin,
    """
      |.N.N
      |R...
      |.N.N
      |..R.
      |""".stripMargin,
    """
      |R...
      |.N.N
      |..R.
      |.N.N
      |""".stripMargin,
    """
      |..R.
      |.N.N
      |R...
      |.N.N
      |""".stripMargin,
    """
      |...R
      |N.N.
      |.R..
      |N.N.
      |""".stripMargin,
    """
      |.R..
      |N.N.
      |...R
      |N.N.
      |""".stripMargin,
    """
      |N.N.
      |.R..
      |N.N.
      |...R
      |""".stripMargin,
    """
      |N.N.
      |...R
      |N.N.
      |.R..
      |""".stripMargin)

  val (eightQueenInput, eightQueenSolutionCount) = Input(rows = 8, columns = 8, queens = 8) -> 92

  val (emptyInput, emptySolutions) = Input(rows = 3, columns = 3) -> Set(
    """
      |...
      |...
      |...
      |""".stripMargin
  )

  val impossibleQuizInput = Input(rows = 8, columns = 8, queens = 9)

  test("Depth first solver should solve the 2K 1R problem") {
    val results = new DFSolver(twoKingsOneRockInput).results.map(board => board.toString).toSet
    results should equal (twoKingsOneResults)
  }

  test("Breadth first solver should solve the 2K 1R problem") {
    val results = new BFSolver(twoKingsOneRockInput).results.map(board => board.toString).toSet
    results should equal (twoKingsOneResults)
  }

  test("Depth first solver should solve the 2R 4N problem") {
    val results = new DFSolver(twoRooksFourKnightsInput).results.map(board => board.toString).toSet
    results should equal (twoRooksFourKnightsResults)
  }

  test("Breadth first solver should solve the 2R 4N problem") {
    val results = new BFSolver(twoRooksFourKnightsInput).results.map(board => board.toString).toSet
    results should equal (twoRooksFourKnightsResults)
  }

  test("Depth first solver should solve the 8Q problem") {
    val results = new DFSolver(eightQueenInput).results.size
    results should equal (eightQueenSolutionCount)
  }

  test("Breadth first solver should solve the 8Q problem") {
    val results = new BFSolver(eightQueenInput).results.size
    results should equal (eightQueenSolutionCount)
  }

  // -- edge cases

  test("Depth first solver should find 1 solution (the empty board) for the empty input") {
    val results = new DFSolver(emptyInput).results.map(board => board.toString).toSet
    results should equal(emptySolutions)
  }

  test("Breadth first solver should find 1 solution (the empty board) for the empty input") {
    val results = new BFSolver(emptyInput).results.map(board => board.toString).toSet
    results should equal(emptySolutions)
  }

  test("Depth first solver should find no solutions for the impossible input") {
    val results = new DFSolver(impossibleQuizInput).results.size
    results should equal(0)
  }

  test("Breadth first solver should find no solutions for the impossible input") {
    val results = new BFSolver(impossibleQuizInput).results.size
    results should equal(0)
  }

  // -- Random tests: results by DFSolver and BFSolver are compared

  val randomInputs: Gen[Input] = {
    val rowGen = Gen.choose(2, 5)

    val pieceGen = Gen.frequency(
      (6, 0),
      (4, 1),
      (2, 2),
      (1, 3))

    for (rows <- rowGen;
         columns <- rowGen;
         kings <- pieceGen;
         queens <- pieceGen;
         rooks <- pieceGen;
         bishops <- pieceGen;
         knights <- pieceGen) yield Input(rows, columns, kings, queens, rooks, bishops, knights)
  }

  test("DFSolver and BFSolver should find the same solutions for any random input") {
    forAll(randomInputs) { input =>
      val dfResults = new DFSolver(input).results.map(_.toString).toSet
      val bfResults = new DFSolver(input).results.map(_.toString).toSet
      bfResults == dfResults
    }.check()
  }
}
