package com.bence.chess

import org.scalatest.{FunSuite, Matchers}

class ChessPieceTest extends FunSuite {

  val (pieceRow: Int, pieceCol: Int) = (4, 4)

  val hLine = (0 until 10).map((pieceRow, _)).toSet - ((pieceRow, pieceCol))
  val vLine = (0 until 10).map((_, pieceCol)).toSet - ((pieceRow, pieceCol))
  val diagonal1 = (0 until 10).map(i => (i, i)).toSet - ((pieceRow, pieceCol))
  val diagonal2 = (0 until 10).map(i => (i, 8 - i)).toSet - ((pieceRow, pieceCol))


  def shouldAttachErrorText(piece: ChessPiece, shouldAttack: Boolean, row: Int, col: Int) =
    s"${piece} at (${pieceRow}, ${pieceCol}) should ${if (shouldAttack) "" else "NOT "}attack field (${row}, ${col})."

  def verifyAttacks(piece: ChessPiece, expectedAttackedFields: Set[(Int, Int)]): Unit = {
    val attacks = piece.attacks(pieceRow, pieceCol) _
    for(row <- 0 until 10; col <- 0 until 10) {
      val expectedAttacks = expectedAttackedFields.contains((row, col))
      assert(attacks(row, col) == expectedAttacks, shouldAttachErrorText(piece, expectedAttacks, row, col))
    }

  }

  test("A king should attack all neighbouring fields and nothing else.") {
    verifyAttacks(King, Set((3, 3), (3, 4), (3, 5), (4, 3), (4, 5), (5, 3), (5, 4), (5, 5)))
  }

  test("A queen should attack all diagonal and straight paths and nothing else.") {
    verifyAttacks(Queen, hLine ++ vLine ++ diagonal1 ++ diagonal2)
  }

  test("A rook should attack all straight paths and nothing else.") {
    verifyAttacks(Rook, hLine ++ vLine)
  }

  test("A bishop should attack all diagonal paths and nothing else.") {
    verifyAttacks(Bishop, diagonal1 ++ diagonal2)
  }

  test("A knight should attack all fields reachable by an L shaped path and nothing else.") {
    val lShapedPaths = Set((-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1))
    verifyAttacks(Knight, lShapedPaths.map {case (vMove, hMove) => (pieceRow + vMove, pieceCol + hMove) })
  }

}
