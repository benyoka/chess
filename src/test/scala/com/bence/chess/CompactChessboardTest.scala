package com.bence.chess

import org.scalatest.{FunSuite, Matchers}

class CompactChessboardTest extends FunSuite with Matchers {

  test("A compact chessboard should print itself correctly.") {
    val expected =
      """
        |K.Q.R.
        |B.N...
        |......
        |......
        |...K.Q
        |.R.B.N
        |""".stripMargin

    val board = ChessBoard.create(6, 6,
      List(
        (King, 0, 0),
        (Queen, 0, 2),
        (Rook, 0, 4),
        (Bishop, 1, 0),
        (Knight, 1, 2),
        (King, 4, 3),
        (Queen, 4, 5),
        (Rook, 5, 1),
        (Bishop, 5, 3),
        (Knight, 5, 5)))

    board.compact.toString should equal(expected)
  }
}
