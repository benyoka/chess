package com.bence.chess

import org.apache.logging.log4j.scala.Logging

object Runner extends Logging {

  def main(args: Array[String]): Unit = {
    val inputs = List(
      Input(rows = 3, columns = 3, kings = 2, rooks = 1),
      Input(rows = 4, columns = 4, rooks = 2, knights = 4),
      Input(rows = 7, columns = 7, kings = 2, queens = 2, bishops = 2, knights = 1)
    )
    logger.info("--- Using BFSolver ----------------------------------------")
    inputs.foreach{ input => runAndPrint(new BFSolver(input)) }
    logger.info("\n\n\n--- Using DFSolver ----------------------------------------")
    inputs.foreach{ input => runAndPrint(new DFSolver(input)) }
  }

  def run(solver: ChessPuzzleSolver): RunResult = {
    val t1 = System.nanoTime()
    val results = solver.results
    val t2 = System.nanoTime()
    RunResult(results, t2 - t1)
  }

  def printResult(solver: ChessPuzzleSolver, result: RunResult): Unit = {
    logger.info(s"${solver.input}, solutions: ${result.boards.size}, time(ns): ${result.time}, time(s): " +
      s"${result.time.toDouble / 1000000000}")
    result.boards.take(10).foreach(board => logger.info(board.toString))
    if (result.boards.size > 10) {
      logger.info("\n...\n")
    }
  }

  def runAndPrint(solver: ChessPuzzleSolver): Unit = {
    printResult(solver, run(solver))
  }

  case class RunResult(boards: List[CompactChessBoard], time: Long)
}

