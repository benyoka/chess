package com.bence

package object chess {

  type ChessPiecePlacement = List[(ChessPiece, Int, Int)]
}
