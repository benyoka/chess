package com.bence.chess

/**
  * Abstract class for chess quiz solvers
  */
abstract class ChessPuzzleSolver(val input: Input) {

  // more powerful pieces are placed first in order to limit the search space
  val initialPiecesToPlace: List[ChessPiece] = (1 to input.queens).map(_ => Queen).toList ++
    (1 to input.rooks).map(_ => Rook).toList ++
    (1 to input.bishops).map(_ => Bishop).toList ++
    (1 to input.kings).map(_ => King).toList ++
    (1 to input.knights).map(_ => Knight).toList


  def results: List[CompactChessBoard]

}
