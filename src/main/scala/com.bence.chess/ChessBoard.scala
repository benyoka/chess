package com.bence.chess

import scala.math.Ordering.Implicits._

/**
  * Represents a chessbord that maintains maximum positions for each piece type. This is required by DFSolver to avoid
  * duplicate elements on the search path.
  *
  * @param rows number of rows
  * @param columns number of columns
  * @param pieces initial chesspiece placement: (row, column) -> piece type
  * @param pieceMaxPositions maximum positions for each piece type. A position is considered higher if it is in a higher
  *                          row or in the same row but in a higher column
  */
class ChessBoard private (val rows: Int, val columns: Int, val pieces: ChessPiecePlacement,
                          val kingMaxPos: (Int, Int) = (0, 0),
                          val queenMaxPos: (Int, Int) = (0, 0),
                          val rookMaxPos: (Int, Int) = (0, 0),
                          val bishopMaxPos: (Int, Int) = (0, 0),
                          val knightMaxPos: (Int, Int) = (0, 0)) {

  assert(rows > 0 && columns > 0, "Chessboard dimensions must be at least 1x1")

  private def this(rows: Int, columns: Int, pieces: ChessPiecePlacement) {
    this(rows, columns, pieces,
      ChessBoard.pieceMaxPos(King, pieces),
      ChessBoard.pieceMaxPos(Queen, pieces),
      ChessBoard.pieceMaxPos(Rook, pieces),
      ChessBoard.pieceMaxPos(Bishop, pieces),
      ChessBoard.pieceMaxPos(Knight, pieces))
  }

  /**
    * @param coords the current position
    * @return the next position (or None if the end of the board is reached)
    */
  def nextPosition(coords: (Int, Int)): Option[(Int, Int)] = coords match {
    case (row, col) =>
      if (col < columns - 1) Some((row, col + 1))
      else if (row < rows - 1) Some((row + 1, 0))
      else None
  }

  /**
    * Tells if (row, column) position is already occupied or placing a chesspiece of the given type on that position
    * would result in an attack.
    */
  def isOccupiedOrInAttack(piece: ChessPiece, row: Int, col: Int): Boolean = pieces.exists {
    case (p, r, c) => (r == row &&  c == col) || p.attacks(r, c)(row, col) || (p != piece && piece.attacks(row, col)(r, c))
  }

  /**
    * Tells if a position is fre (not occupied and not being attacked by any of the pieces on the board)
    */
  def isFree(row: Int, col: Int): Boolean = !pieces.exists {
    case (p, r, c) => (r, c) == (row, col) || p.attacks(r, c)(row, col)
  }

  /**
    * Finds the next available position for a given chess piece from a given starting position, provided it exists
    */
   def findGoodPlaceFrom(piece: ChessPiece, fromPosition: (Int, Int)): Option[(Int, Int)] = {
    if (!isOccupiedOrInAttack(piece, fromPosition._1, fromPosition._2))
      Some(fromPosition)
    else nextPosition(fromPosition) match {
      case None => None
      case pos @ Some((row, col)) if !isOccupiedOrInAttack(piece, row, col) => pos
      case Some(pos) => findGoodPlaceFrom(piece, pos)
    }
  }

  private def positionToken(row: Int, col: Int): Char = {
    pieces.filter {
      case (p, r, c) => (r == row && c == col) || p.attacks(r, c)(row, col)
    } match {
      // field is occupied or attacked
      case list if list.nonEmpty => list.find { case (p, r, c) => r == row && c == col } match {
        case Some((p, r, c)) => p.token // occupied
        case None => '*' // attacked
      }
      // field is free
      case Nil => '.'
    }
  }

  private def rowToString(row: Int) =
    (0 until columns).map(col => positionToken(row, col)).mkString

  override def toString(): String = (0 until rows).map(rowToString).mkString("[", "][", "]")

  def toMultilineString(): String = (0 until rows).map(rowToString).mkString("\n", "\n", "\n")

  /**
    * Determines the minimum coordinates for a piece to place. This is required to ensure that chess pieces are not
    * placed in lower positions than existing pieces of the same type in order to avoid duplicate search steps.
    *
    * For pieces already existing on the board, the coordinates of the piece on the highest position will be returned
    * instead of the next position in order to spare using Option's
    *
    * @param piece the piece to place
    * @return the minimum coordinates to start placement at
    */
  def minCoordsToPlace(piece: ChessPiece): (Int, Int) = piece match {
    case King => kingMaxPos
    case Queen => queenMaxPos
    case Rook => rookMaxPos
    case Bishop => bishopMaxPos
    case Knight => knightMaxPos
  }

  /**
    * Places the given piece on the board at the given position, resulting in a new board.
    */
  def place(piece: ChessPiece, row: Int, col: Int): ChessBoard = {
    val position = (row, col)
    val newPieces = (piece, row, col) :: pieces
    val currentMaxPos = minCoordsToPlace(piece)
    val newMaxPos = if (position > currentMaxPos) position else currentMaxPos
    new ChessBoard(rows, columns, newPieces,
      if (piece == King) newMaxPos else kingMaxPos,
      if (piece == Queen) newMaxPos else queenMaxPos,
      if (piece == Rook) newMaxPos else rookMaxPos,
      if (piece == Bishop) newMaxPos else bishopMaxPos,
      if (piece == Knight) newMaxPos else knightMaxPos)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[ChessBoard]

  override def equals(other: Any): Boolean = other match {
    case that: ChessBoard =>
      (that canEqual this) &&
        rows == that.rows &&
        columns == that.columns &&
        pieces.toSet == that.pieces.toSet && // set conversion
        kingMaxPos == that.kingMaxPos &&
        queenMaxPos == that.queenMaxPos &&
        rookMaxPos == that.rookMaxPos &&
        bishopMaxPos == that.bishopMaxPos &&
        knightMaxPos == that.knightMaxPos
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(rows, columns, pieces.toSet /* ! */, kingMaxPos, queenMaxPos, rookMaxPos, bishopMaxPos, knightMaxPos)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  def compact: CompactChessBoard = new CompactChessBoard(this)
}

object ChessBoard {
  def initial(rows: Int, columns: Int): ChessBoard = new ChessBoard(rows, columns, Nil)

  def create(rows: Int, columns: Int, pieces: ChessPiecePlacement) = new ChessBoard(rows, columns, pieces)

  private def pieceMaxPos(pieceType: ChessPiece, pieces: ChessPiecePlacement): (Int, Int) =
    pieces.collect { case (piece, row, col) if piece == pieceType => (row, col) } match {
      case Nil => (0, 0)
      case list => list.max
    }

}