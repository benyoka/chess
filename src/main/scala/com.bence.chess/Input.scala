package com.bence.chess

case class Input(rows: Int, columns: Int, kings: Int = 0, queens: Int = 0, rooks: Int = 0, bishops: Int = 0, knights: Int = 0) {
  assert(rows > 0 && columns > 0, "There must be at least one row and column.")
  assert(kings >= 0 && queens >= 0 && rooks >=0 && bishops >=0 && knights >= 0, "Negative number must not be allowed for piece count.")
}
