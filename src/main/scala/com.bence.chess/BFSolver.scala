package com.bence.chess

import org.apache.logging.log4j.scala.Logging

/**
  * Implements a breadth first solution to the chess quiz
  */
class BFSolver(input: Input) extends ChessPuzzleSolver(input) with Logging {

  /**
    * Represents a state in the search space. A state consists of the chess board and the remaining pieces to place
    */
  case class State(val board: ChessBoard, val piecesToPlace: List[ChessPiece]) {
    def isTerminal: Boolean = piecesToPlace.isEmpty
  }

  /**
    * For any input state, returns a set of possible next states by placing the next chess piece.
    */
  def placeNextPiece(state: State): List[State] = state.piecesToPlace match {
    case Nil => Nil
    case piece :: tail =>
      val (minRow, minCol) = state.board.minCoordsToPlace(piece)
      (for (row <- minRow until input.rows;
            col <- 0 until input.columns if (row > minRow || col >= minCol) && !state.board.isOccupiedOrInAttack(piece, row, col))
        yield new State(state.board.place(piece, row, col), tail )).toList
  }

  /**
    * A stream of sets of states that can be reached from a set of input states. Elements in the set are in the same
    * distance from the input states
    */
  def from(states: List[State]): List[List[State]] = states match {
    case s if s.isEmpty => Nil
    case s =>
      val next = s.flatMap(placeNextPiece)
      logger.debug(s"${s} -> ${next}")
      s :: from(next)
  }

  override def results(): List[CompactChessBoard] = {
    val initialState = new State(ChessBoard.initial(input.rows, input.columns), initialPiecesToPlace)
    val allStates = from(initialState :: Nil)
    val finalStates = allStates.find( states => states.exists(_.isTerminal)).getOrElse(Set.empty)
    finalStates.map(_.board.compact).toList
  }
}
