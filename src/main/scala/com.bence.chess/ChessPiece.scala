package com.bence.chess

sealed trait ChessPiece {
  final def attacks(myRow: Int, myColumn: Int)(enemyRow: Int, enemyColumn: Int): Boolean =
    // In real chess we should also examine if the path between the two pieces is clear (except for knights) but now it
    // does not matter because in such cases a piece on the move path would be attacked resulting in an invalid setup
    (myRow, myColumn) != (enemyRow, enemyColumn) && canMoveTo(myRow, myColumn)(enemyRow, enemyColumn)

  protected def canMoveTo(fromRow: Int, fromColumn: Int)(toRow: Int, toColumn: Int): Boolean

  protected def isOnSameLine(myRow: Int, myColumn: Int)(enemyRow: Int, enemyColumn: Int) =
    myRow == enemyRow || myColumn == enemyColumn

  protected def isOnSameDiagonal(myRow: Int, myColumn: Int)(enemyRow: Int, enemyColumn: Int) =
    math.abs(myRow - enemyRow) == math.abs(myColumn - enemyColumn)

  val ordinal: Byte

  def token: Char

}

object ChessPiece {
  def fromOrdinal(ordinal: Byte): ChessPiece = ordinal match {
    case King.ordinal => King
    case Queen.ordinal => Queen
    case Rook.ordinal => Rook
    case Bishop.ordinal => Bishop
    case Knight.ordinal => Knight
  }
}

case object Queen extends ChessPiece {
  override def canMoveTo(fromRow: Int, fromColumn: Int)(toRow: Int, toColumn: Int): Boolean =
    isOnSameLine(fromRow, fromColumn)(toRow, toColumn) || isOnSameDiagonal(fromRow, fromColumn)(toRow, toColumn)

  override def token: Char = 'Q'

  override val ordinal: Byte = 1
}

case object Rook extends ChessPiece {
  override def canMoveTo(fromRow: Int, fromColumn: Int)(toRow: Int, toColumn: Int): Boolean =
    isOnSameLine(fromRow, fromColumn)(toRow, toColumn)

  override def token: Char = 'R'

  override val ordinal: Byte = 2

}

case object Bishop extends ChessPiece {
  override def canMoveTo(fromRow: Int, fromColumn: Int)(toRow: Int, toColumn: Int): Boolean =
    isOnSameDiagonal(fromRow, fromColumn)(toRow, toColumn)

  override def token: Char = 'B'

  override val ordinal: Byte = 3
}

case object King extends ChessPiece {
  override def canMoveTo(fromRow: Int, fromColumn: Int)(toRow: Int, toColumn: Int): Boolean =
    math.abs(fromRow - toRow) <= 1  && math.abs(fromColumn - toColumn) <= 1

  override def token: Char = 'K'

  override val ordinal: Byte = 0
}

case object Knight extends ChessPiece {
  override def canMoveTo(fromRow: Int, fromColumn: Int)(toRow: Int, toColumn: Int): Boolean =
    (math.abs(fromRow - toRow) == 2  && math.abs(fromColumn - toColumn) == 1) ||
      (math.abs(fromRow - toRow) == 1  && math.abs(fromColumn - toColumn) == 2)

  override def token: Char = 'N'

  override val ordinal: Byte = 4

}
