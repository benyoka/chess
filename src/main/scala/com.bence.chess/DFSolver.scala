package com.bence.chess

import org.apache.logging.log4j.scala.Logging


/**
  * A solution based on depth first walk
  */
class DFSolver(input: Input) extends ChessPuzzleSolver(input) with Logging {

  /**
    * Represent a possible placement over an existing chess board and a list of figures to place
    * @param board the existing chess board
    * @param pieces the remaining pieces to place. The head of the list is the next piece to place
    * @param rowToPlace row to place the next piece
    * @param columnToPlace column to place the next piece
    */
  case class Step(val board: ChessBoard, val pieces: List[ChessPiece], val rowToPlace: Int, val columnToPlace: Int) {
    def isTerminal: Boolean = pieces.isEmpty || pieces.tail.isEmpty

    /**
      * @return The next step that represents placing the same chess piece in the next available (not occupied and not
      *         in attack) position, if such step exists.
      */
    def tryPlaceSamePieceInNextPosition: Option[Step] = {
      val nextStep = if (pieces.isEmpty)
        None
      else {
        for( nextPos <- board.nextPosition((rowToPlace, columnToPlace));
             nextGoodPos <- board.findGoodPlaceFrom(pieces.head, nextPos))
          yield new Step(board, pieces, nextGoodPos._1, nextGoodPos._2)
      }
      logger.debug(s"${this} tryPlaceSamePieceInNextPosition -> ${nextStep}")
      nextStep
    }

    /**
      * @return A step that represents adding the current chess piece to the board in the current position and taking the
      *         next piece to place, provided it exists.
      */
    def tryPlaceNextPiece: Option[Step] = {
      val nextStep = if (isTerminal) {
        None
      }
      else {
        val newBoard = doStep
        val newPieces = pieces.tail
        newBoard.findGoodPlaceFrom(newPieces.head, newBoard.minCoordsToPlace(newPieces.head)) match {
          case Some((newRow, newCol)) => Some(new Step(newBoard, newPieces, newRow, newCol))
          case None => None
        }
      }
      logger.debug(s"${this} tryPlaceNextPiece -> ${nextStep}")
      nextStep
    }

    /**
      * @return a new chessboard with the next piece added
      */
    def doStep: ChessBoard = if (pieces.isEmpty) board else board.place(pieces.head, rowToPlace, columnToPlace)

    override def toString: String = s"Step {board: ${board}, pieces: ${pieces}, row: ${rowToPlace}, col: ${columnToPlace}}"
  }

  /**
    * depth first walk on the possible steps
    * @param step
    * @param finalSteps final steps accumulator
    * @return accumulated final steps
    */
  def visit(step: Step, finalBoards: List[CompactChessBoard]): List[CompactChessBoard] = {
    val finalBoards2 = if (step.isTerminal) step.doStep.compact :: finalBoards else finalBoards
    // step down in the search graph
    val finalBoards3 = step.tryPlaceNextPiece match {
      case Some(newStep) => visit(newStep, finalBoards2)
      case None => finalBoards2
    }
    // step to sibling in the search graph
    step.tryPlaceSamePieceInNextPosition match {
      case Some(newStep) => visit(newStep, finalBoards3)
      case None => finalBoards3
    }
  }

  override def results = {
    val initialStep = new Step(ChessBoard.initial(input.rows, input.columns), initialPiecesToPlace, 0, 0)
    visit(initialStep, Nil)
  }

}

