package com.bence.chess

/**
  * Compact chessboard implementation for memory-efficient storing of end results.
  * Rows and columns are not stored.
  *
  * Supports only 127 rows and columns.
  */
class CompactChessBoard(chessBoard: ChessBoard) {

  assert(chessBoard.rows < 128 && chessBoard.columns < 128, s"Maximum 127 rows and columns are supported")

  val data = Array.ofDim[Byte](chessBoard.pieces.length, 3)
  val rows: Byte = chessBoard.rows.toByte
  val columns: Byte = chessBoard.columns.toByte

  chessBoard.pieces.zip(0 until data.length).foreach {
    case ((piece, row, col), idx) =>
      data(idx)(0) = piece.ordinal
      data(idx)(1) = row.toByte
      data(idx)(2) = col.toByte
  }

  def positionToken(row: Int, col: Int): Char =
    data.
      find( placement => placement(1) == row && placement(2) == col).
      map( placement => ChessPiece.fromOrdinal(placement(0)).token ).
      getOrElse('.')

  private def rowToString(row: Int, columns: Int) =
    (0 until columns).map(col => positionToken(row, col)).mkString

  override def toString: String = (0 until rows).map(row => rowToString(row, columns)).mkString("\n", "\n", "\n")

}

