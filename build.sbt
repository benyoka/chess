name := "chess"

version := "1.2.8"

scalaVersion := "2.12.8"
organization := "com.bence"


libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.11.2"
libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.2"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api-scala_2.12" % "11.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"

logBuffered in Test := false

Compile / run / fork := true
outputStrategy := Some(StdoutOutput)

// can be 1G when only the DFSolver is used in Runner
javaOptions += "-Xmx2G"