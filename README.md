## Chess Quiz Solver

1. Use **sbt run** to run the solvers on example data. The program will print on the console. All runs will be timed.
If  there are more than ten solutions for an input, only the first ten boards will be printed.
2. Use **sbt test** to run the unit tests.

### Performance
**Quiz:** 7x7 board: K=2 Q=2 R=0 B=2 N=1, using BFSolver

**Machine:** Intel© Core™ i5-3320M CPU @ 2.60GHz × 2, 8GB RAM, Linux

**from IntelliJ:** solutions: 3063828, time(ns): 19302471626, time(s): 19.302471626

### Improvement ideas
- Make DFSolver return a stream of solutions
- Partition the search space for DFSolver to as many chunks as compute cores
